<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        General Form Elements
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">General Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
           <!--/.col (left) -->
        <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Kategori</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <?php
            include 'conn.php';
            $id=@$_GET['id'];$tipe=@$_GET['tipe'];
            if($id!=''){
            $query=mysql_query("SELECT * FROM t_kategori WHERE id_kategori='$id'");
            while($data=mysql_fetch_array($query)){
            $nama=$data['nama_kategori'];
            $idField='<input type="hidden" name="id" value="'.$id.'">';
              }
            }else{
              $nama='';
              $tipe='1';
              $idField='';
            }
            ?>
            <form class="form-horizontal" method="post" action="simpan.php">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="nama" placeholder="Nama Kategori" value="<?php echo $nama; ?>">
                  </div>
                </div>
                
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <?php echo $idField; ?>
                <input type="hidden" name="tipe" value="<?php echo $tipe; ?>">
                <button type="submit" class="btn btn-info pull-right">Simpan</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->
          <!-- general form elements disabled -->
          
          <!-- /.box -->
        </div>
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Kategori</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                include 'conn.php';
                $no=1;
                $query=mysql_query("select * from t_kategori");
                while($data=mysql_fetch_array($query)){
                echo "<tr>
                <td>$no</td>
                <td>$data[nama_kategori]</td>
                <td><a href='user.php?konten=kategori&id=$data[id_kategori]&tipe=2' class='btn'><i class='fa fa-edit'></i> Edit</a> <a href='simpan.php?id=$data[id_kategori]&tipe=3' class='btn'><i class='fa fa-remove'></i> Hapus</a></td>
                </tr>";
                $no++;
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama Kategori</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
